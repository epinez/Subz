Keep and eye on your subscriptions and contracts. Let you remind of cancelation periods.

<ul>
<li>Add subscriptions and details like costs, billing interval, contract durations and cancelation period</li>
<li>Show all subscriptions and their costs</li>
<li>Show days until next billing</li>
<li>Switch subscription cost overview to daily, weekly, monthly and yearly</li>
<li>Modify and delete subscriptions</li>
<li>Search for specific subscriptions</li>
<li>Sort subscriptions by name, costs, etc.</li>
<li>Optional reminder for reaching cancelation period of subscriptions</li>
</ul>